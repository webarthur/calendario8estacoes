var EightSeasonsCalendar = {

  // Total days of an year should be 365 or 366
  totalYearDays: null,

  // Month background color
  bgColor: 'Silver',

  // Define de months of the calendar
  months: [
    {
      name: 'Verão',
      starts: '21/12',
      icon: 'fas fa-sun',
      text: ''
    },
    {
      name: 'Pré-Outono',
      starts: '03/02',
      icon: 'fas fa-tree',
      text: ''
    },
    {
      name: 'Outono',
      starts: '20/03',
      icon: 'fas fa-leaf',
      text: ''
    },
    {
      name: 'Pré-Inverno',
      starts: '05/05',
      icon: 'fas fa-mitten',
      text: ''
    },
    {
      name: 'Inverno',
      starts: '21/06',
      icon: 'far fa-snowflake',
      text: ''
    },
    {
      name: 'Pré-Primavera',
      starts: '06/08',
      icon: 'fab fa-pagelines',
      text: ''
    },
    {
      name: 'Primavera',
      starts: '22/09',
      icon: 'fas fa-spa',
        text: ''
    },
    {
      name: 'Pré-Verão',
      starts: '07/11',
      icon: 'fas fa-cloud-sun',
      text: ''
    }
  ],

  // The date that starts the calendar (month/day)
  starts: '/12/21',

  // The date that starts the calendar (month/day)
  weekDays: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],

  // Constellation dates
  stars: {
    '20/01': '#ffcf84', // Capricorn
    '16/02': 'LightSalmon', // Aquarius
    '11/03': '#ffb9b9', // Pisces
    '18/04': 'Thistle', // Aries
    '13/05': 'Lavender', // Taurus
    '21/06': 'LightSteelBlue', // Gemini
    '20/07': 'PowderBlue', // Cancer
    '10/08': '#74e7c1', // Leo
    '16/09': 'LightGreen', // Virgo
    '30/10': 'YellowGreen', // Libra
    '23/11': 'LightGreen', // Scorpio
    '29/11': 'Khaki', // Ophiuchus
    '17/12': '#f4ea83', // Sagittarius
    '21/12': '#f4ea83' // Sagittarius (BUGFIX 1)
  },

  dates: {},

  // Configure calendar variables
  setup (data) {
    for (var prop in data) {
      if (data.hasOwnProperty(prop)) {
        this[prop] = data[prop]
      }
    }
    return this
  },

  // Get the day background color
  getBgColor (gregDate) {
    this.bgColor = this.stars[gregDate] || this.bgColor
    return this.bgColor
  },

  // From: https://pt.stackoverflow.com/questions/21739/como-validar-data-levando-em-conta-ano-bissexto
  leapYear (year) {
    return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0) ? 1 : 0
  },

  // dateDiff (date1, date2) {
  //   return (date1.getTime() - date2.getTime()) / (24 * 60 * 60 * 1000)
  // },

  // Generate calendar to a HTML node, example:
  // EightSeasonsCalendar.generate('#calendar')
  generate (target, year) {
    if (!year) {
      year = new Date().getFullYear() -1
      // year = 2018
    }

    // Update total days of the calendar
    this.totalYearDays = 365 + this.leapYear(year)

    var dt = new Date(year + EightSeasonsCalendar.starts)
    var totalYearDays = 365 + this.leapYear(year)
    var html = ''
    var month = 1
    var lastMonth = 1
    var day = 1
    var days = 0

    var today = new Date()
    today = String(today.getDate()).padStart(2, '0') + '/' + String(today.getMonth() + 1).padStart(2, '0') + '/' + today.getFullYear()

    console.log('today',today)

    var dates = []
    while (days < EightSeasonsCalendar.totalYearDays) {
      var data = EightSeasonsCalendar.months[month - 1]

      // Starts the month
      html += `
        <table class="EightSeasonsCalendarMonth">
          <tr>
            <th class="esc-month-header" colspan="7">
              <div>
                <div style="text-align:center">
                  <i style="font-size: 1.8em" class="fa-fw ${data.icon}"></i>
                  <span style="display: inline-block; font-size: 1.4em">
                    <span>
                      0${month}
                    </span>
                    <b>
                      ${data.name}
                    </b>
                  </span>
                </div>
                <div>
                  ${data.text}
                </div>
              </div>
            </th>
            </tr>
          <tr>
      `

      // Add week days
      for (var i = 0; i < 7; i++) {
        html += `<td class="esc-weekdays">${this.weekDays[i]}</td>`
      }
      html += '</tr><tr>'

      // Fill start blocks of a month
      var col = dt.getDay()
      for (var i = 0; i < col; i++) {
        html += '<td class="esc-empty-column"></td>'
      }

      lastMonth = month
      dates = []
      while (lastMonth === month && days < EightSeasonsCalendar.totalYearDays) {
        var date = {
          day: String(dt.getDate()).padStart(2, '0'),
          month: String(dt.getMonth() + 1).padStart(2, '0'),
          year: dt.getFullYear()
        }

        // Get full gregorian date as string
        var fullGregDate = date.day + '/' + date.month + '/' + date.year

        // Next month
        if (day !== 1) {
          var isNextMonth = EightSeasonsCalendar.months.find(function (month) {
            return month.starts === date.day + '/' + date.month
          })
          if (isNextMonth) {
            lastMonth = +1
            break
          }
        }

        // Next week
        if (col === 7) {
          html += '</tr><tr>'
          col = 0
        }

        // Update columns counter
        col += 1

        // Update days counter
        days += 1

        // Add footer description
        var sDate = EightSeasonsCalendar.dates[fullGregDate]
        if (sDate) {
          if (sDate[0] === ':') {
            var sDay = sDate.split(' ')[0].split(':')[1]
            var sText = sDate.replace(/^:[^ ]+/, '')
            dates.push(`<b>${sDay}</b> ${sText}`)
          }
          else {
            dates.push(`<b>${String(day).padStart(2, '0')}</b> ${sDate}`)
          }
        }

        // Check if it is a special date
        var extraClass = ''
        if (EightSeasonsCalendar.dates[fullGregDate]) {
          extraClass += ' special'
        }

        // Check if the date is today
        if (fullGregDate === today) {
          extraClass += ' today'
        }

        var gregDate = date.day + '/' + date.month
        var bgColor = EightSeasonsCalendar.getBgColor(gregDate)

        // Check if the date is sunday
        if (dt.getDay() === 0) {
          extraClass += ' sunday'
        }

        // The day
        html += `
          <td class="esc-day ${extraClass}" style="background: ${bgColor}">
            <div>
              ${day} <br>
              <small>${gregDate}</small>
            </div>
          </td>
        `

        // Prepare to next day
        day += 1
        dt.setDate(dt.getDate() + 1)
      }

      // Fill end blocks of a month
      for (var i = 0; i < 7 - col; i++) {
        html += '<td class="esc-empty-column"></td>'
      }

      // Close table tag
      html += `
        </tr>
        <tr>
          <td class="esc-month-footer" colspan="7">
            ${dates.join('<br>')}
          </td>
        </tr>
        </table>
      `
      // Prepare to next month
      day = 1
      month += 1
    }

    // Append the HTML
    document.querySelector(target).innerHTML = `
      <div style="display:flex; flex-wrap: wrap">
        ${html}
      </div>
    `
  }
}
