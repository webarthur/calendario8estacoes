# Calendário das 8 Estações

Calendário Solar baseado em 8 estações: as 4 tradicionais (primavera, verão, outono e inverno) e mais 4 estações que são uma subdivisão das estações tradicionais.

Visite: https://webarthur.gitlab.io/calendario8estacoes/

## Características

* Marcar os dias de início de cada estação
* Caracterizar a sozonalidade de cada estação
* Marcar os dias de chuvas de meteoros
* Marcar os dias de lua cheia e nova
* Marcar os dias em que o Sol entra nas constelações (astronomicamente)
* Marcar com uso de cores a época de cada constelação

## Motivação

O solstício de versão é uma dia especial, o Sol inicia um novo ciclo! A data do solstício era comemorada como início do ano novo por muitas civilizações antigas (maias, índios norte-americanos, egípcios, chineses, mesopotamios, vikings, gregos, etc). Nosso calendário atual, também conhecido como calendário gregoriano, foi inventado pelo papa papa Gregorio XIII em 1582 para corrigir uma falha de cálculo no caledário Juliano (calendário usado na época). O calendário Juliano, por sua vez, foi uma reforma do calendário romano que teve raízes na civilização babilônica (povo este que definiu a duração dos meses baseado no movimento lunar, criou os dias da semana, as horas, os minutos e os segundos). De lá pra cá muita coisa se perdeu e ao contrário dos antigos, nosso calendário hoje não reflete nenhum evento astronômico, além de seu uso ter sido imposto pela perseguição católica aplicando leis, prisões e outras punições. O calendário que iniciou seu objetivo como uma ferramenta para acompanhar o cosmos perdeu seu sentido, no entanto, ainda existem outros 40 calendários independentes ainda em uso, a maior parte deles em países do oriente. O calendário gregoriano nada nos diz sobre nós mesmos. Além disto, o solstício também foi a origem do festival Natalis Solis Invicti, comemoração do deus Sol por "enfrentar" o inverno e ter seu retorno glorioso. É desta comemoração, por exemplo, que surge o conhecido "natal", uma adaptação a festa "pagã" que o catolicismo nos impôs também...

## Intenção

O Calendário das 8 Estações não tem a intenção de ser um substituto ao calendário civil atual (calendário gregoriano), mas um calendário complementar a vida por acompanhar os ciclos das estações assim como cada estação se comporta, os movimentos lunares, os eventos astronômicos e época de safras. Cada cidade possui suas próprias sazonalidades, devido a isto, há um calendário base e um calendário para cada cidade. Caso sua cidade ainda não tenha o calendário das 8 estações, sinta-se a vontade para contribuir =)

## TODO

* Marcar a época das frutas
